# 🧶 基 于 layui 的 Toast 通 知 单 消 息 拓 展

[layui官网](https://layui.dev/)

[在线测试](https://stackblitz.com/edit/layui-toast-exp?file=index.html)

## 配置

toast.js

```js
// 配置
layui.config({
    base: "/model/" // toast.js 文件的拓展目录
})
```

## API

| api | 描述 |
| ---- | ---- |
| let toast = layui.toast| 获得 `toast` 模块 |
| 弹出: | - |
| toast.show(content,option) | 弹出吐司消息 |
| 关闭: | - |
| toast.close(index) | 关闭吐司消息 |

### 弹出吐司消息

`toast.show(content,option);`

- 参数 `content` , 类型 `string` : 弹出的内容，可以是html字符串

- 参数 `option` , 类型 `object` : 基础属性配置

option属性

| 属性名 | 描述 | 类型 | 默认 |
| ---- | ---- | ---- | ---- |
| title | 标题 | string | '提示' |
| icon | 提示图标 <br> 支持传入`0~3`,如下： <br> ` toast.show('成功提示', {icon: 1}); ` <br> 可传字符串,如下: <br> `toast.show('成功提示', {icon: '<i class="layui-icon layui-icon-moon"></i>'});` <br>    | string/number | '' |
| time | 自动关闭所需的毫秒数。 如 `time: 5000` ，即代表 5 秒后自动关闭。<br> 如果为 `0` 则不关闭 | number | 5000 |
| class | 用于给吐司容器追加 css 类名，以便更好地扩展表格样式 | string | '' |
| offset | 吐司的偏移坐标。 支持以下可选值<br> `offset:"rt"` 右上 <br> `offset:"rb"` 右下 <br> `offset:"lt"` 左上 <br> `offset:"lb"` 左下 | string | 'rt' |
| success | 弹出吐司消息成功回调  | function | function (index, elem) {} |

```js
success: function (index, elem) {
    console.log("索引值:", index) // 索引值
    console.log("自身元素对象:", elem) // jquery对象
}
```

| 属性名 | 描述 | 类型 | 默认 |
| ---- | ---- | ---- | ---- |
| cancel | 关闭的回调函数,点击关闭按钮也会触发 | function | function (index, elem) {} |

```js
cancel: function (index, elem) {
    console.log("索引值:", index) // 索引值
    console.log("自身元素对象:", elem) // jquery对象

    return true // 如果返回false则不关闭，定时关闭也会取消
}
```

### 关闭吐司消息

`toast.close(index)`

- 参数 `index` ,类型：`string|number` toast的索引值，或者传入 `all` 关闭全部吐司消息

```js
// 指定
let toast_index  = toast.show("这是一个指定的吐司消息,不自动消失", { time: 0 })

// 关闭指定吐司
toast.close(toast_index)
// 关闭所有吐司消息
toast.close("all")
```