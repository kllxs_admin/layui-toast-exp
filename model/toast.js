layui.define(["jquery"], function (exports) {

    //jq
    let $ = layui.$

    // 当前数
    let alert_group = []
    let index = 0

    // 默认图标组
    const icon_group = [
        `<i class="layui-icon layui-icon-tips layui-font-orange"></i>`,
        `<i class="layui-icon layui-icon-success layui-font-green"></i>`,
        `<i class="layui-icon layui-icon-error layui-font-red"></i>`,
        `<i class="layui-icon layui-icon-question layui-font-blue"></i>`
    ]


    // 右上
    if (!$(".toast.rt").length) {
        $("body").append(`<div class="toast rt"></div>`)
        $(".toast.rt").css({
            "right": "10px",
            "top": "10px",
            "flex-direction": "column"
        })
    }
    // 右下
    if (!$(".toast.rb").length) {
        $("body").append(`<div class="toast rb"></div>`)
        $(".toast.rb").css({
            "right": "10px",
            "bottom": "10px",
            "flex-direction": "column-reverse"
        })
    }
    // 左上
    if (!$(".toast.lt").length) {
        $("body").append(`<div class="toast lt"></div>`)
        $(".toast.lt").css({
            "left": "10px",
            "top": "10px",
            "flex-direction": "column"
        })
    }
    // 左下
    if (!$(".toast.lb").length) {
        $("body").append(`<div class="toast lb"></div>`)
        $(".toast.lb").css({
            "left": "10px",
            "bottom": "10px",
            "flex-direction": "column-reverse"
        })
    }

    $("body").append(`<style>
        .toast{
            position: fixed;
            display: flex;
            flex-wrap: nowrap;
            gap: 10px;
        }

        .toast-box {
            max-width: 330px;
            min-width: 150px;
            display: flex;
            flex-direction: row;
            align-items: flex-start;
            padding: 14px 10px;
            box-sizing: border-box;
            box-shadow: 0 2px 12px 0 rgba(0, 0, 0, .1);
            border-radius: 8px;
            position: relative;
            z-index: 9999;
            backdrop-filter: blur(5px);
        }

        .toast.rt .toast-box,.toast.rb .toast-box{
            animation: slideFromRight .4s ease-in-out;
        }
        
        @keyframes slideFromRight {
            from {
                right: -100%;
            }

            to {
                right: 0;
            }
        }

        .toast.lt .toast-box,.toast.lb .toast-box{
            animation: slideFromLeft .4s ease-in-out;
        }
        
        @keyframes slideFromLeft {
            from {
                left: -100%;
            }

            to {
                left: 0;
            }
        }

        .toast-box .toast-group {
            width: 100%;
            padding-left: 8px;
            box-sizing: border-box;
        }

        .toast-box .toast-group .toast-content {
            font-size: 14px;
            padding-top: 6px;
        }

        .toast-box .toast-icon i {
            font-size: 24px;
        }

        .toast-box .toast-close {
            cursor: pointer;
        }

        .toast-box .toast-close i {
            font-size: 16px;
        }
</style>`)

    // 关闭按钮
    $("body").delegate(".toast-close", "click", function () {
        let id = $(this).parent().attr("id")
        toastClose(id)
    })

    /**
     * 关闭
     * @param {number} id 
     */
    const toastClose = function (id) {
        let obj
        for (let y = 0; y < alert_group.length; y++) {
            if (typeof alert_group[y] !== "undefined" && alert_group[y].id == id) {
                obj = alert_group[y]
            } else {
                continue
            }
        }
        // 触发关闭回调
        let cancel = obj.cancel(id, $(`.toast-box[id="${id}"]`))
        if (cancel === false) {
            if (typeof obj.timeOut !== "undefined") {
                clearTimeout(obj.timeOut)
            }
            return false
        }
        let i = 0;
        // 定时向指定位置移动并删除
        let closeTime = setInterval(function () {
            switch (obj.offset) {
                case "rt":
                    $(`.toast-box[id="${obj.id}"]`).css("right", `-${i}%`)
                    break;
                case "rb":
                    $(`.toast-box[id="${obj.id}"]`).css("right", `-${i}%`)
                    break;
                case "lt":
                    $(`.toast-box[id="${obj.id}"]`).css("left", `-${i}%`)
                    break;
                case "lb":
                    $(`.toast-box[id="${obj.id}"]`).css("left", `-${i}%`)
                    break;
            }
            if (i >= 100) {
                // 删除
                $(`.toast-box[id="${obj.id}"]`).remove()
                // 清理
                clearTimeout(closeTime)
                if (typeof obj.timeOut !== "undefined") {
                    clearTimeout(obj.timeOut)
                }
            }
            i++
        }, 1)
    }

    /**
     * 添加
     * @param {string} msg html
     * @param {object} data 对象
     */
    const toastAdd = async function (msg, data, id) {
        let time
        $(".toast." + data.offset).append(`
        <div class="toast-box ${data.class}" id="${id}">
            <div class="toast-icon">
                ${data.icon}
            </div>
            <div class="toast-group">
                <h2 class="toast-title layui-font-16">${data.title}</h2>
                <div class="toast-content">
                    ${msg}
                </div>
            </div>
            <div class="toast-close">
                <i class="layui-icon layui-icon-close layui-font-gray"></i>
            </div>
        </div>`)
        data.success(id, $(`.toast-box[id="${id}"]`))
        if (data.time !== 0) {
            // 设置定时器
            time = setTimeout(function () {
                toastClose(id)
            }, data.time)
            data.timeOut = time
        }
        data.id = id
        alert_group.push(data)
    }

    /**
     * 吐司消息
     * @param {string} content 内容
     * @param {object} option 配置信息
     */
    let toast = {
        show: function (content, option = {}) {
            let id = index
            option.title = option.title || "提示"
            option.icon = icon_group[option.icon] || ""
            if (typeof option.time !== "number") {
                option.time = 5000
            }
            option.time = option.time
            option.class = option.class || ""
            option.offset = option.offset || "rt"
            option.success = option.success || function (id, elem) { }
            option.cancel = option.cancel || function (id, elem) { return true }

            // 添加吐司
            toastAdd(content, option, id)
            index++
            return id
        },
        close: function (id = "all") {
            if (id == "all") {
                for (let i = 0; i < alert_group.length; i++) {
                    toastClose(alert_group[i].id)
                }
            } else {
                toastClose(id)
            }
        }
    }

    exports("toast", toast)
})